require 'descriptive_statistics'
require './lib/salary'
require './lib/fix_salary_mistakes'
require './lib/markdown_helper'


class AgeStatisticsReport < SurveyReport
  include MarkdownHelper

  def run responses
    report_title "Age report"

    raw = raw_ages responses
    by_range = ages_by_range responses

    section_title "Age summary"

    puts """With #{raw.statistics[:number].to_i} responses, the average age of a C&S
    member is approximately #{raw.statistics[:mean].round} years of age. The youngest responder
    is #{raw.statistics[:min].to_i} years of age and the oldest is #{raw.statistics[:max].to_i} years of age,
    spanning #{raw.statistics[:range].to_i} years.
    The highest number of responders are #{raw.statistics[:mode].to_i} years of age. Two-thirds
    of respondents are between #{(raw.statistics[:mean]-raw.statistics[:standard_deviation]).round}
    and #{(raw.statistics[:mean]+raw.statistics[:standard_deviation]).round}.
    """

  end

  def raw_ages responses
    section_title "Raw age analysis"
    analyzer = AgeAnalyzer.new(responses)
    puts AnalyzerDecorator.new(analyzer).to_s
    analyzer
  end

  RANGES = {
    "18 to 22" => Proc.new {|age| (18..22).include? age },
    "23 to 27" => Proc.new {|age| (23..27).include? age },
    "28 to 32" => Proc.new {|age| (28..32).include? age },
    "33 to 39" => Proc.new {|age| (33..39).include? age },
    "40 to 49" => Proc.new {|age| (40..49).include? age },
    "50 to 59" => Proc.new {|age| (50..59).include? age },
    "60+" => Proc.new {|age| (60..120).include? age  },
    "Unspecified" => Proc.new {|age| age.nil? || age == "" }
  }
  def ages_by_range responses
    grouped_by_age = {}

    responses.each do |resp|
      age = age_bucket_for resp[Q_AGE]
      unless grouped_by_age.has_key? age
        grouped_by_age[age] = []
      end
      grouped_by_age[age] << resp
    end

    section_title "Age Range Counts"
    table_header_row ["Age Range", "Count"]
    RANGES.keys.each do |range|
      array_to_table [range, grouped_by_age[range].size]
    end

    section_title "Percentage of demographics"
    table_header_row ["Age Range", "Percentage"]
    RANGES.keys.each do |range|
      array_to_table [range, "%2.2f%%" % (100*(grouped_by_age[range].size.to_f / responses.size.to_f))]
    end

    section_title "Age range-specific salary statistics"
    RANGES.keys.map do |range|
      gresponses = grouped_by_age[range]
      unless gresponses.nil?
        subsection_title "Salary report for #{range}"
        gresponses = ExcludeUnderMinimumWage.new.fix gresponses
        analysis = SalaryAnalyzer.new gresponses
        stats = analysis.statistics
        puts
        puts AnalyzerDecorator.new(analysis).to_s
        stats
      end
    end

  end
  def age_bucket_for age
    RANGES.each do |label, check|
      return label if check.call(age)
    end
  end
end



class AgeAnalyzer

  def initialize responses
    @responses = responses
    @ages = responses.map { |resp| resp[Q_AGE] }.reject { |resp| resp.nil? || resp == "" }
  end

  def statistics
    @ages.descriptive_statistics
  end
end


REPORTS << AgeStatisticsReport
