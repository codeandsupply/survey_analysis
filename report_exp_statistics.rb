require 'descriptive_statistics'
require './lib/salary'
require './lib/fix_salary_mistakes'
require './lib/fix_experience_mistakes'
require './lib/markdown_helper'


class ExperienceStatisticsReport < SurveyReport
  include MarkdownHelper

  def run responses
    report_title "Experience report"

    question_was Q_EXPERIENCE

    raw = raw responses
    by_range responses

    section_title "Experience summary"

    puts """With #{raw.statistics[:number].to_i} responses, the average experience of a C&S
    member is approximately #{raw.statistics[:mean].round} years. The least experienced responder
    has #{raw.statistics[:min]} years and the most veteran has #{raw.statistics[:max].to_i} years.
    The highest number of responders have #{raw.statistics[:mode].to_i} years of experience. Two-thirds
    of respondents are between #{(raw.statistics[:mean]-raw.statistics[:standard_deviation]).round}
    and #{(raw.statistics[:mean]+raw.statistics[:standard_deviation]).round} years of experience.
    """

    puts """If we broke down respondents into four groups – entry-level, junior, senior, and veteran – by each occupying
    25% of the respondents, then
    an entry-level member has 0 to #{raw.statistics[:q1].round} years of experience,
    a junior member has #{raw.statistics[:q1].round} to #{raw.statistics[:q2].round} years of experience,
    a senior member has #{raw.statistics[:q2].round} to #{raw.statistics[:q3].round} years of experience,
    and a veteran member has #{raw.statistics[:q3].round} to #{raw.statistics[:max].round} years of experience.
    """

  end

  def raw responses
    section_title "Raw experience analysis"
    analyzer = ExperienceAnalyzer.new(responses)
    puts AnalyzerDecorator.new(analyzer).to_s
    analyzer
  end

  RANGES_SPECIAL_CASES = {
    "30+ years" => Proc.new {|number| (30..120).include? number },
  }

  RANGES = [(0...3), (3...6), (6...9), (9...20), (20...30)].map do |range|
    ["#{range.first} to #{range.last} years", Proc.new {|number| range.include? number }]
  end.to_h.merge(RANGES_SPECIAL_CASES)

  def by_range responses
    analyzer = ExperienceAnalyzer.new(responses)
    grouped = {}

    analyzer.responses.each do |resp|
      bucket = bucket_for resp[Q_EXPERIENCE]
      unless grouped.has_key? bucket
        grouped[bucket] = []
      end
      grouped[bucket] << resp
    end

    section_title "Experience Range Counts"
    table_header_row ["Experience Range", "Count"]
    RANGES.keys.each do |range|
      array_to_table [range, grouped[range].size]
    end

    section_title "Percentage of demographics"
    table_header_row ["Experience Range", "Percentage"]
    RANGES.keys.each do |range|
      array_to_table [range, "%2.2f%%" % (100*(grouped[range].size.to_f / responses.size.to_f))]
    end

    section_title "Experience range-specific salary statistics"
    grouped.map do |range, gresponses|
      subsection_title "Salary report for #{range}"
      gresponses = ExcludeUnderMinimumWage.new.fix gresponses
      analysis = SalaryAnalyzer.new gresponses
      stats = analysis.statistics
      puts
      puts AnalyzerDecorator.new(analysis).to_s
      stats
    end

    section_title "Year of experience per employer"
    puts "This section shows respondents stayed at an employer on average."
    puts
    years_per_employer = analyzer.responses.map do |resp|
      num_employers = resp[Q_NUMEMPLOYERS]
      if num_employers.nil? || (num_employers.class == String && num_employers.empty? ) || num_employers == 30 || num_employers == 0
        nil
      else
        ype = resp[Q_EXPERIENCE].to_f / num_employers.to_f
        begin
          ype.round
        rescue
          STDERR.puts "ERROR: #{ype} = #{resp[Q_EXPERIENCE]} / #{resp[Q_NUMEMPLOYERS]}"
          nil
        end
      end
    end.reject(&:nil?)
    puts AnalyzerDecorator.new(YearsPerEmployerAnalyzer.new(years_per_employer)).to_s
    puts
    years_per_employer_grouped = years_per_employer.group_by(&:to_i).map { |count,list| [count, list.size] }.to_h.sort.to_h
    STDERR.puts "YPE: #{years_per_employer_grouped}"
    table_header_row ["Years per employer", "Count"]
    years_per_employer_grouped.each do |years,count|
      array_to_table [years, count]
    end




  end
  def bucket_for number
    RANGES.each do |label, check|
      return label if check.call(number)
    end
  end
end

class YearsPerEmployerAnalyzer
  def initialize arry
    @arry = arry
  end
  def statistics
    @arry.descriptive_statistics
  end
end


class ExperienceAnalyzer

  attr_reader :responses

  def initialize responses
    exps_unfixed = responses.reject { |resp| resp[Q_EXPERIENCE].nil? || resp[Q_EXPERIENCE] == "" || resp[Q_EXPERIENCE] == 0.0}
    fixed = FixExperienceMistakes.new.fix exps_unfixed
    @responses = fixed
  end

  def statistics
    @responses.map{|resp| resp[Q_EXPERIENCE]}.descriptive_statistics
  end
end


REPORTS << ExperienceStatisticsReport
