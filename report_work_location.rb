class WorkLocationReport < SurveyReport
  def run responses
    analyzer = WorkLocationAnalyzer.new responses

    report_title "Work location report"
    question_was Q_OFFICE

    puts """This question allowed multiple answers, so counts will exceed total
    number of responses. The goal here is to show how many of respondents
    reported activity in the given work location."""
    puts
    puts "* #{analyzer.removed} responses were not considered because no work location was chosen."
    puts "* #{analyzer.dropped} categories were dropped because there were not enough responses for them to be statistically significant."

    section_title "Work Location Counts"
    table_header_row ["Answer", "Count"]
    analyzer.grouped.each do |answer,resps|
      array_to_table [answer, analyzer.grouped[answer].size]
    end

    section_title "Percentage of demographics"
    table_header_row ["Answer", "Percentage"]
    analyzer.grouped.each do |answer,resps|
      array_to_table [answer, "%2.2f%%" % (100*(analyzer.grouped[answer].size.to_f / responses.size.to_f))]
    end

    section_title "Work location-specific salary statistics"

    puts "Keep in mind that this should be read as \"If I am working from this location, these statistics may apply to me.\""

    analyzer.grouped.map do |answer, gresponses|
      subsection_title "Salary report for #{answer}"
      gresponses = ExcludeUnderMinimumWage.new.fix gresponses
      analysis = SalaryAnalyzer.new gresponses
      stats = analysis.statistics
      puts
      puts AnalyzerDecorator.new(analysis).to_s
      stats
    end

  end
end

REPORTS << WorkLocationReport

class WorkLocationAnalyzer
  MINIMUM_NUMBER_FOR_SIGNIFICANCE = 3
  attr_reader :grouped, :removed, :dropped

  def explode resp
    resp[Q_OFFICE].split(";").map{|role| make_uniform(role) }
  end

  def make_uniform role
      WorkLocationCategorizer.rename role
  end

  def initialize responses
    @responses = responses
    @removed = 0
    @dropped = 0
    @grouped = {}

    responses.each do |resp|
      unless resp[Q_OFFICE].nil? || resp[Q_OFFICE].empty?
        explode(resp).each do |role|
          unless @grouped.has_key? role
            @grouped[role] = []
          end
          @grouped[role] << resp
        end
      else
        @removed += 1
      end
    end

    @grouped = @grouped.sort_by{|answer, resps| resps.size }.reverse.to_h
    before_filter_count = @grouped.size
    @grouped = @grouped.reject {|answer, resps| resps.size <= MINIMUM_NUMBER_FOR_SIGNIFICANCE }
    @dropped = before_filter_count - @grouped.size

  end
end

class WorkLocationCategorizer
   NO_RENAME = [
     "Home",
     "Co-working space",
     "Coffee shop",
     "Employer's office",
     "Coffee shop",
     "Customer office",
   ]

   RENAME = {
     "" => ["Unknown"]
   }.invert.map{|entries,use| entries.map { |entry| {entry => use} } }.flatten # it's too late the <center> cannot hold

   def self.rename given
     return given if NO_RENAME.member? given
     rename = RENAME.find {|pair| pair[given] }
     unless rename.nil?
       return rename.values.first
     end
     given
   end

end
