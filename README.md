# C&S Survey Analyzer

Google Forms tools are great until they're not when a survey has a lot of text
fields. This tool helps clean and correct entries, then spits out some analysis.

> **DO NOT COMMIT SURVEY RESULTS TO THIS REPO.** This repo is for the analysis
> script _only_.

## Running

    ruby analyze.rb <results.csv>

If the `CONSOLE` environment variable is set, then the analyzer will drop to a
pry interactive REPL for manual analysis.
