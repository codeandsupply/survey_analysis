require 'csv'

fail "Pass the path to the report CSV!" unless ARGV[0]
response_file = ARGV[0]

responses = CSV.read response_file, headers: true, converters: :all

responses.freeze

Q_TIME='Timestamp'
Q_GENDER='What is your gender?'
Q_AGE='What is your age? '
Q_CHILDREN='Do you have children in your household under the age of 13?'
Q_EXPERIENCE='How many years have you been employed in a software role?'
Q_EDUCATION='What is your level of education?'
Q_SKILLS='Where did you learn the skills you use?'
Q_TITLE='What is your job title?'
Q_ROLE='What is your role?'
Q_SALARY='What is your expected yearly pre-tax salary at your current job?'
Q_FAIRLY='Do you feel fairly compensated for your role?'
Q_ADDITIONAL='What total additional compensation might you receive?'
Q_EQUITY='Please specify any equity you may have in your employer. '
Q_PTO='How many days of Paid Time Off do you have, including major holidays? '
Q_ONCALL='Are you ever subject to on-call duty?'
Q_ONCALLCOMP='If you have on-call duty, describe the compensation you receive for being on-call.'
Q_NUMEMPLOYERS='How many different software employers have you had? '
Q_LIVE='In what municipality do you live most of the time?'
Q_WORK='In what municipality do you work most of the time?'
Q_HQ='Where is your employer headquartered?'
Q_COMMUTE='How far is your commute?'
Q_OFFICE='What type of location do you work from?'


class Fixnum
  def thousands
    to_s.reverse.gsub(/...(?=.)/,'\&,').reverse
  end
end

if ENV['CONSOLE']
  require 'pry'
  binding.pry
end

require "./lib/markdown_helper"
class SurveyReport
  include MarkdownHelper

  def report_title title
    puts "\n" + header(title, 2)
  end
  def section_title title
    puts "\n" + header(title, 3)
  end
  def subsection_title title
    puts "\n" + header(title, 4)
  end
  def subsubsection_title title
    puts "\n" + header(title, 5)
  end
  def question_was question
    puts "The question was:\n> #{question}\n\n"
  end
end

FIXES = []
class SurveyFix; end


REPORTS = []

MEMBERSHIP_COUNTS = { "Meetup" => 2966, "Slack" => 920, "Website" => 231 }

Dir.glob("./report*.rb") { |file| require file.gsub('.rb', '') }
puts "\# Code & Supply Compensation Survey Results"
puts "* The target population was [Code & Supply](https://www.codeandsupply.co) members."
puts """* The survey was distributed via [C&S Meetup](https://www.meetup.com/Pittsburgh-Code-Supply/),
the [C&S mailing list](http://eepurl.com/bs8wJD),
[@codeandsupply](https://twitter.com/codeandsupply) on Twitter,
and in the [C&S Slack team](https://www.codeandsupply.co/chat)."""
puts "* There were **#{responses.size}** total responses during this month-long survey."
puts "    * This represents approximately:"
MEMBERSHIP_COUNTS.map { |thing,count| puts "        * %2.2f%% of our #{count.thousands} #{thing} members" % (count/responses.size.to_f) }.join(", ")
puts "* The results are reflective of C&S members and _may_ also be reflective of software professionals in Pittsburgh."

puts "Some things to know about the reports contained herein:"

puts "* There are **#{REPORTS.size}** reports describing the result."
puts "* Not all responses are usable for every report."
puts ""

puts "\#\# Fixes"
puts "* Some responses contained what were deemed to be typographical errors and were corrected."
puts "* Some responses needed to be normalized in order to be processed."

Dir.glob("./lib/fix*.rb") { |file| require file.gsub('.rb', '') }
puts "* #{FIXES.size} fixes or normalizations were applied."
FIXES.each do |cls|
  instance = cls.new
  responses = instance.fix responses
end

REPORTS.each do |cls|
  instance = cls.new
  instance.run responses.dup
end
