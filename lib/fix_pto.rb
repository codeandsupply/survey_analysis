class FixPtoMistakes < SurveyFix
  UNLIMITED = -1.0
  # must return the modified responses
  def fix responses
    fix_count = 0
    fixed_responses = responses.map { |resp|
      resp_exp = resp[Q_PTO]
      if resp_exp.class == Fixnum
        resp[Q_PTO] = resp_exp.to_f
      elsif resp_exp.class == Float
        resp[Q_PTO] = resp[Q_PTO]
      elsif resp_exp.class == String
        fixed = fix_string resp_exp
        if fixed.class != Float
          STDERR.puts "Response could not be converted to float:\n\t[#{fixed}] which isa #{fixed.class}"
          nil
        end
        resp[Q_PTO] = fixed
        fix_count += 1
      end
      resp
    }
    puts "* #{fix_count} PTO entries were normalized.\n\n"
    fixed_responses
  end

  ONE_OFF_TABLE = {
    "None" => 0.0,
    "15+unlimited sick time" => 15.0,
    "17 + Unlimited Sick" => 17.0,
    "unlimited, I guess?" => UNLIMITED,
    "Self-employed, so none" => 0.0,
    "54 (24 vacation, 12 paid holidays, 18 sick days)" => 54.0,
    "\"Unlimited\"  (29)" => 29.0,
    "15 vacation days, Christmas to New Years, most major holidays, 12 sick days" => 38.0,
    "Two weeks vacation, unlimited sick time, paid holidays" => 16.0,
    "unlimited, 15 days guideline" => UNLIMITED,
    "15 vacation. 10 sick" => 25.0,
    "15 PTO + holidays" => 21.0,
    "34 (15 vacation, 7 sick, 12 holiday)" => 34.0,
    "30 (17 PTO, 3 floating, 10 paid holidays)" => 30.0,
    "Zero, but… self-employed" => 0.0,
    "32: 6 paid sick days; 8 paid holidays; 18 vacation days" => 32.0,
    "All national holidays, and two weeks off" => 16.0,
    "18 lousy days" => 18.0,
    "unlimited. Open policy" => UNLIMITED,
    "About 3-4 weeks" => 20.0,
    "14 or thereabouts" => 14.0,
    "20 vacation days unlimited sick time" => 20.0,
    "21 days of PTO, plus extra for sick time, and we get the week between Christmas and New Years" => 26.0,
    "30 PTO plus unlimited sick" => 30.0,
    "Infinite. Self-employed, 100% ownership." => 0.0,
    "6 or 7,unlimited sick days, 80 hour vacation time" => 10.0,
    "I work ~1000 hours/year, so unlimited" => UNLIMITED,
    "24 (10 PTO, 14 holiday)" => 24.0,
    "20 and all federal holidays" => 26.0,
    "none, am paid hourly" => 0.0,
    "11 Holidays, 3 Personal Days, 10 Sick Days, 10 Vacation Days" => 34.0,
    "Three weeks plus major holidays" => 21.0,
    "24 PTO, 1 FH, 7 H" => 24.0, #dunno
    "freelance so none" => 0.0
  }

  def fix_string resp
    unlimited_match = /^[U|u]nlimited$/.match resp.strip
    unless unlimited_match.nil?
      STDERR.puts "PTO: Unlimited"
      return UNLIMITED
    end

    weeks_match = /^(\d\d?|\d.\d) [W|w]eeks?$/.match resp.strip
    unless weeks_match.nil?
      weeks = weeks_match[1].to_f / 12.0
      STDERR.puts "WEEKS FIX: #{resp} -> #{weeks}"
      return weeks
    end

    days_match = /^(\d\d?|\d.\d) [D|d]ays?$/.match resp.strip
    unless days_match.nil?
      days = days_match[1].to_f / 12.0
      STDERR.puts "DAYS FIX: #{resp} -> #{days}"
      return days
    end

    if ONE_OFF_TABLE.member? resp.strip
      result = ONE_OFF_TABLE[resp.strip]
      STDERR.puts "LOOKUP FIX: #{resp} -> #{result}"
      return result
    end

    resp
  end
end

FIXES << FixPtoMistakes
