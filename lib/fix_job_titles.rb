class FixJobTitles < SurveyFix

  TABLE = {
    "Software Engineer" => [
          /^[s|S]oftware [E|e]ngineer$/,
          /^Developer$/i,
          /^Software Developer$/i,
          /^Systems programmer$/i,
          /^Staff Software Engineer$/i,
          /^Software Engineer I$/i,
          /^Software Development Engineer$/i,
          /^Advisory Software Engineer$/i,
          /^Developer$/i,
          /^DevOps\/Software engineer$/i
    ],
    "Senior Software Engineer" => [
          /^[s|S]enior [s|S]oftware [E|e]nginee[r|t]$/i,
          /^[s|S][R|r].? [s|S]oftware [E|e]ngin(eer)?$/i,
          /^[s|S]enior [s|S]oftware [D|d]eveloper$/i,
          /^Software Engineer IV$/i,
          /^Senior Developer$/i,
          /^Sr. Software Eng$/i,
          /^Senior Software Development Engineer$/i
    ],
    "CTO" => [
      /^CTO$/i,
      /^Chief Technology Officer$/i
    ],
    "Consultant" => [
      /^Consultant$/i
    ],
    "Web Developer" => [
      /^Web developer$/i
    ],
    "Applications Engineer" => [
      /^Applications? [D|d]eveloper$/i,
      /^Applications? [E|e]ngineer$/i
    ],
    "Senior Applications Engineer" => [
      /^Senior Applications? [D|d]eveloper$/i,
      /^Senior Applications? [E|e]ngineer$/i
    ],
    "Prefer not to answer" => [
      /$^/
    ],
    "VP" => [
      /^VP/i
    ],
    "QA/Test Engineer" => [
      /^(QA|Test) Engineer$/i,
      /^Software Quality Engineer$/i
    ],
    "Lead or Principal Engineer" => [
      /^(Lead|Principal) Software (Developer|Engineer)$/i,
      /^(Lead|Principal) Engineer$/i
    ],
    "Front-end Engineer" => [
      /^Front[- ]?[E|e]nd (Developer|Engineer)$/i
    ]


  }.invert

  def fix responses
    fix_count = 0
    fixed_responses = responses.map do |resp|
      resp_title = resp[Q_TITLE].strip
      #STDERR.puts "Considering #{resp_title}"
      result = TABLE.find do |regexes, result|
        regexes.any? do |regex|
          #STDERR.puts "\tcomparing #{regex} against #{resp_title} for #{result}"
          match = regex.match resp_title
          #STDERR.puts match
          match
        end
      end
      #STDERR.puts result
      unless result.nil?
         resp[Q_TITLE] = result[1]
         fix_count += 1
      end
      resp
    end
    puts "* #{fix_count} job titles were made uniform.\n\n" if fix_count > 0
    fixed_responses #.reject {|resp| resp[Q_TITLE].empty? || resp[Q_TITLE].nil? }
  end
end

FIXES << FixJobTitles
