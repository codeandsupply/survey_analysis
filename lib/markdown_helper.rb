module MarkdownHelper
  def table_header_row headers
    separator = []
    headers.each { |h| separator << "-" * [h.size, 3].max }
    array_to_table headers
    array_to_table separator
  end

  def array_to_table arry
    puts "|#{arry.join('|')}|"
  end

  def header content, level = 1
    octothorpes = "\#" * level
    "#{octothorpes} #{content}"
  end
end
