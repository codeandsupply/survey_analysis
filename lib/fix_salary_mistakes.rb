require './lib/exclude_under_minimum_wage.rb'

class FixSalaryMistakes < SurveyFix
  # must return the modified responses
  def fix responses, options = {exclude_under_minimum_wage: false }
    to_analyze = responses
    fixed = 0
    puts
    unless ENV['DONT_FIX_SALARIES']
      puts "* Salary entry error corrections:"
      to_analyze = to_analyze.map do |r|
        salary = r[Q_SALARY]
        if salary < 1000 && 0 < salary
          puts "    * #{salary} is likely error, multiplying by 1000"
          r[Q_SALARY] = salary * 1000
          fixed = fixed + 1
        end
        if salary == 1000
          puts "    * #{salary} is likely error, multiplying by 100"
          r[Q_SALARY] = salary * 100
          fixed = fixed + 1
        end
        r
      end
    end
    puts "    * #{fixed} salaries fixed in set" if fixed > 0

    if options[:exclude_under_minimum_wage]
      excluder = ExcludeUnderMinimumWage.new
      to_analyze = excluder.fix to_analyze
    end

    to_analyze
  end
end

FIXES << FixSalaryMistakes
