require 'descriptive_statistics'

class SalaryAnalyzer

  def initialize responses
    @responses = responses
    @salaries = responses.map { |resp| resp[Q_SALARY] }
  end

  def statistics
    @salaries.descriptive_statistics
  end
end

require './lib/markdown_helper'
class AnalyzerDecorator
  include MarkdownHelper

  MINIMUM_NUMBER_FOR_SIGNIFICANCE = 2

  def initialize analyzer
    @analyzer = analyzer
  end

  def to_s
    count = @analyzer.statistics[:number]
    if count <= MINIMUM_NUMBER_FOR_SIGNIFICANCE
      puts """*Unfortunately, this grouping does not have enough data
      to provide statistically significant information. These
      results may not be sufficiently reflective of the population.*
      """
      return ""
    end

    table_header_row ["Statistic","Quantity"]

    @analyzer.statistics.collect do |stat,val|
      case stat
      when :mean
        "| **#{stat.to_s}** | **#{val}** |"
      when :variance
        "| #{stat.to_s} | #{val} |"
      when :number
        "| #{stat.to_s} | %d |" % val
      else
        "| #{stat.to_s} | %2.2f |" % val unless val.nil?
     end
    end.join("\n")
  end
end
