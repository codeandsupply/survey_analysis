require 'descriptive_statistics'
require './lib/salary'
require './lib/markdown_helper'


class CommuteTimeReport < SurveyReport
  include MarkdownHelper

  def run responses
    report_title "Commute Time report"

    question_was Q_COMMUTE

    raw = raw responses
    by_range responses

    section_title "Commute Time summary"

    puts """With #{raw.statistics[:number].to_i} responses, the average commute time of a C&S
    member is approximately #{raw.statistics[:mean].round} minutes. The closest responder
    has a commute of #{raw.statistics[:min]} minutes and the farthest has a commute of #{raw.statistics[:max].to_i} minutes.
    The highest number of responders have #{raw.statistics[:mode].to_i} minute commute. Two-thirds
    of respondents are between #{(raw.statistics[:mean]-raw.statistics[:standard_deviation]).round}
    and #{(raw.statistics[:mean]+raw.statistics[:standard_deviation]).round} minutes for their commute.
    """
  end

  def raw responses
    section_title "Raw commute time analysis"
    analyzer = CommuteTimeAnalyzer.new(responses)
    puts "#{analyzer.filtered} responses were removed because they did not answer this question."
    puts
    puts AnalyzerDecorator.new(analyzer).to_s
    analyzer
  end

  RANGES_SPECIAL_CASES = {
    # no one has this
    #"120+ minutes" => Proc.new {|number| (120..999).include? number },
  }

  RANGES = [(0...5), (5...10), (10...20), (20...30), (30...45), (45...60),(60...90),(90...121)].map do |range|
    ["#{range.first} to #{range.last-1} minutes", Proc.new {|number| range.include? number }]
  end.to_h.merge(RANGES_SPECIAL_CASES)

  def by_range responses
    analyzer = CommuteTimeAnalyzer.new(responses)
    grouped = {}

    analyzer.responses.each do |resp|
      bucket = bucket_for resp[Q_COMMUTE]
      unless grouped.has_key? bucket
        grouped[bucket] = []
      end
      grouped[bucket] << resp
    end

    section_title "Commute Time Counts"
    table_header_row ["Commute Time", "Count"]
    RANGES.keys.each do |range|
      if grouped[range].nil?
        STDERR.puts "Commute range #{range} is nil."
        count = 0
      else
        count = grouped[range].size
      end
      array_to_table [range, count]
    end

    section_title "Percentage of demographics"
    table_header_row ["Commute Time", "Percentage"]
    RANGES.keys.each do |range|
      array_to_table [range, "%2.2f%%" % (100*(grouped[range].size.to_f / responses.size.to_f))]
    end

    section_title "Commute Time-specific salary statistics"
    grouped.map do |range, gresponses|
      subsection_title "Salary report for #{range}"
      gresponses = ExcludeUnderMinimumWage.new.fix gresponses
      analysis = SalaryAnalyzer.new gresponses
      stats = analysis.statistics
      puts
      puts AnalyzerDecorator.new(analysis).to_s
      stats
    end
  end
  def bucket_for number
    RANGES.each do |label, check|
      return label if check.call(number)
    end
  end
end

class CommuteTimeAnalyzer

  attr_reader :responses

  def initialize responses
    @initial_count = responses.size
    @responses = responses.reject { |resp| resp[Q_COMMUTE].nil? || resp[Q_COMMUTE] == "" }
    @filtered_count = @responses.size
  end

  def filtered
    @initial_count - @filtered_count
  end

  def statistics
    @responses.map{|resp| resp[Q_COMMUTE]}.descriptive_statistics
  end
end


REPORTS << CommuteTimeReport
