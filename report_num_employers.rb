class NumberOfEmployersReport < SurveyReport

  def run responses
    report_title "Number of Employers report"
    question_was Q_NUMEMPLOYERS

    #XXX: there are all kinds of duplicate work being done in here, no time to fix it right now
    # The whole report is only adding at most 76 ms to execution.

    analysis = NumberOfEmployersAnalyzer.new responses

    grouped = responses.group_by {|resp| resp[Q_NUMEMPLOYERS] }.reject{|answer,resps| answer.nil? || (answer.class == String && answer.empty? ) || answer == 30 }.sort.to_h

    puts "* #{responses.size - analysis.statistics[:number].to_i} responses were not considered because there was either no answer or the answer reflected freelance engagements."

    section_title "Number of Employers Counts"
    table_header_row ["Answer", "Count"]
    grouped.each do |answer,resps|
      array_to_table [answer, grouped[answer].size]
    end

    section_title "Percentage of demographics"
    table_header_row ["Answer", "Percentage"]
    grouped.each do |answer,resps|
      array_to_table [answer, "%2.2f%%" % (100*(grouped[answer].size.to_f / responses.size.to_f))]
    end

    section_title "Statistical analysis of employer counts"
    puts AnalyzerDecorator.new(analysis).to_s

  end

end

class NumberOfEmployersAnalyzer
  attr_reader :responses
  def initialize responses
    @responses = responses
  end

  def statistics
    num_employers = @responses.map{|resp| resp[Q_NUMEMPLOYERS] }.reject{|answer,resps| answer.nil? || (answer.class == String && answer.empty? ) || answer == 30 }
    num_employers.descriptive_statistics
  end
end


REPORTS << NumberOfEmployersReport
